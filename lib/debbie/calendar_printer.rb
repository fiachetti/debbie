require "debbie/calendar"

module Debbie
  class CalendarPrinter
    attr_reader :calendar
    attr_reader :calendar_config
    attr_reader :num_weeks
    attr_reader :weeks_per_month

    def initialize(calendar, num_weeks: nil, weeks_per_month: nil, calendar_config: calendar.calendar_config)
      @calendar        = calendar
      @calendar_config = calendar_config
      @num_weeks       = num_weeks       || calendar_config.weeks_per_year
      @weeks_per_month = weeks_per_month || calendar_config.weeks_per_year
    end

    def to_s
      result = []

      result << "| Mon | Tue | Wed | Thu | Fri | Sat | Sun |"
      result << weeks.each_with_index.map { |week, current_week|
        week_separator(current_week) +
          "|" +
          week.map { |day| current_day(day) }.join("|") +
          "|"
      }.take(num_weeks)

      result.join("\n") + "\n"
    end

    def weeks
      (1..calendar_config.days_per_year).each_slice(calendar_config.days_per_week)
    end

    def week_separator(current_week)
      separator = "|-----+-----+-----+-----+-----+-----+-----|\n"
      (current_week % weeks_per_month == 0 ? separator : "")
    end

    def current_day(day)
      [
        calendar.day == day ? "(" : " ",
        "%3i" % day,
        calendar.day == day ? ")" : " ",
      ].join
    end
  end
end
