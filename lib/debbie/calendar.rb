require "./lib/debbie/calendar_config/twelve_week_year"

module Debbie
  class Calendar
    attr_reader :now
    attr_reader :calendar_config

    def initialize(now=Time.now, calendar_config: TwelveWeekYear.new)
      @now             = now
      @calendar_config = calendar_config
    end

    def year
      @year ||= days_passed / years + 1
    end

    def week
      @week ||= days_passed / weeks % weeks_per_year + 1
    end

    def day
      @day ||= days_passed % years + 1
    end

    private

    def days_passed
      @days_passed ||= (now - start_date).to_i / days(1)
    end

    def years(n=1)
      n * weeks * weeks_per_year
    end

    def weeks(n=1)
      n * days_per_week
    end

    def days(n=1)
      60*60*24
    end

    def start_date
      calendar_config.start_date
    end

    def weeks_per_year
      calendar_config.weeks_per_year
    end

    def days_per_week
      calendar_config.days_per_week
    end

    def days_per_year
      calendar_config.days_per_year
    end

  end
end
