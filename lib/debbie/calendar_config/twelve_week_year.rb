module Debbie
  class TwelveWeekYear
    attr_reader :start_date
    def initialize(start_date: Time.new(2018, 1, 1))
      @start_date = start_date
    end

    def weeks_per_year
      12
    end

    def days_per_week
      7
    end

    def days_per_year
      weeks_per_year * days_per_week
    end
  end
end
