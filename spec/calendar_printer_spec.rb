require "./lib/debbie/calendar_printer"

module Debbie
  RSpec.describe CalendarPrinter do
    let(:start_date) { Time.new(2018, 1, 1) }

    it "returns a calendar string" do
      now = Time.new(2018, 1, 1)
      cal = Calendar.new(now)

      printer = CalendarPrinter.new(cal)

      expected = <<~CAL
        | Mon | Tue | Wed | Thu | Fri | Sat | Sun |
        |-----+-----+-----+-----+-----+-----+-----|
        |(  1)|   2 |   3 |   4 |   5 |   6 |   7 |
        |   8 |   9 |  10 |  11 |  12 |  13 |  14 |
        |  15 |  16 |  17 |  18 |  19 |  20 |  21 |
        |  22 |  23 |  24 |  25 |  26 |  27 |  28 |
        |  29 |  30 |  31 |  32 |  33 |  34 |  35 |
        |  36 |  37 |  38 |  39 |  40 |  41 |  42 |
        |  43 |  44 |  45 |  46 |  47 |  48 |  49 |
        |  50 |  51 |  52 |  53 |  54 |  55 |  56 |
        |  57 |  58 |  59 |  60 |  61 |  62 |  63 |
        |  64 |  65 |  66 |  67 |  68 |  69 |  70 |
        |  71 |  72 |  73 |  74 |  75 |  76 |  77 |
        |  78 |  79 |  80 |  81 |  82 |  83 |  84 |
      CAL

      expect(printer.to_s).to eql(expected)
    end

    it "returns a calendar string" do
      now = Time.new(2018, 1, 1)
      cal = Calendar.new(now)

      printer = CalendarPrinter.new(cal, num_weeks: 3)

      expected = <<~CAL
        | Mon | Tue | Wed | Thu | Fri | Sat | Sun |
        |-----+-----+-----+-----+-----+-----+-----|
        |(  1)|   2 |   3 |   4 |   5 |   6 |   7 |
        |   8 |   9 |  10 |  11 |  12 |  13 |  14 |
        |  15 |  16 |  17 |  18 |  19 |  20 |  21 |
      CAL

      expect(printer.to_s).to eql(expected)
    end

    it "returns a calendar string" do
      now = Time.new(2018, 1, 1)
      cal = Calendar.new(now)

      printer = CalendarPrinter.new(cal, num_weeks: 1)

      expected = <<~CAL
        | Mon | Tue | Wed | Thu | Fri | Sat | Sun |
        |-----+-----+-----+-----+-----+-----+-----|
        |(  1)|   2 |   3 |   4 |   5 |   6 |   7 |
      CAL

      expect(printer.to_s).to eql(expected)
    end

    it "returns a calendar string" do
      now = Time.new(2018, 1, 3)
      cal = Calendar.new(now)

      printer = CalendarPrinter.new(cal, num_weeks: 1)

      expected = <<~CAL
        | Mon | Tue | Wed | Thu | Fri | Sat | Sun |
        |-----+-----+-----+-----+-----+-----+-----|
        |   1 |   2 |(  3)|   4 |   5 |   6 |   7 |
      CAL

      expect(printer.to_s).to eql(expected)
    end

    it "separate by months" do
      now = Time.new(2018, 1, 1)
      cal = Calendar.new(now)

      printer = CalendarPrinter.new(cal, weeks_per_month: 4)

      expected = <<~CAL
        | Mon | Tue | Wed | Thu | Fri | Sat | Sun |
        |-----+-----+-----+-----+-----+-----+-----|
        |(  1)|   2 |   3 |   4 |   5 |   6 |   7 |
        |   8 |   9 |  10 |  11 |  12 |  13 |  14 |
        |  15 |  16 |  17 |  18 |  19 |  20 |  21 |
        |  22 |  23 |  24 |  25 |  26 |  27 |  28 |
        |-----+-----+-----+-----+-----+-----+-----|
        |  29 |  30 |  31 |  32 |  33 |  34 |  35 |
        |  36 |  37 |  38 |  39 |  40 |  41 |  42 |
        |  43 |  44 |  45 |  46 |  47 |  48 |  49 |
        |  50 |  51 |  52 |  53 |  54 |  55 |  56 |
        |-----+-----+-----+-----+-----+-----+-----|
        |  57 |  58 |  59 |  60 |  61 |  62 |  63 |
        |  64 |  65 |  66 |  67 |  68 |  69 |  70 |
        |  71 |  72 |  73 |  74 |  75 |  76 |  77 |
        |  78 |  79 |  80 |  81 |  82 |  83 |  84 |
      CAL

      expect(printer.to_s).to eql(expected)
    end
  end
end
