require "./lib/debbie/calendar_config/twelve_week_year"

module Debbie
  RSpec.describe TwelveWeekYear do
    context "defaults" do
      it do
        expect(TwelveWeekYear.new.start_date).to eql(Time.new(2018, 1, 1))
      end

      it do
        expect(TwelveWeekYear.new.weeks_per_year).to eql(12)
      end

      it do
        expect(TwelveWeekYear.new.days_per_week).to eql(7)
      end

      it do
        expect(TwelveWeekYear.new.weeks_per_year).to eql(12)
      end

      it do
        expect(TwelveWeekYear.new.days_per_year).to eql(84)
      end

    end
  end
end
