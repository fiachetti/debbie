require 'simplecov'
SimpleCov.start

require "bundler/setup"
require "debbie"

require "awesome_print"

RSpec.configure do |config|
  config.example_status_persistence_file_path = ".rspec_status"

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
