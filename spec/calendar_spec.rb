require "./lib/debbie/calendar"

module Debbie
  RSpec.describe Calendar do
    it "defaults to a TwelveWeekYear config" do
      expect(Calendar.new.calendar_config).to be_a(TwelveWeekYear)
    end

    context "year" do
      it "is on year 1" do
        cal = Calendar.new(Time.new(2018, 3, 25))

        expect(cal.year).to eql(1)
      end

      it "is still on year 1 after exactly 12 weeks" do
        cal = Calendar.new(Time.new(2018, 3, 26))

        expect(cal.year).to eql(2)
      end

      it "is on year 2 after 12 weeks and one day" do
        cal = Calendar.new(Time.new(2018, 3, 26))

        expect(cal.year).to eql(2)
      end

      it "is on year 2 after 24 weeks" do
        cal = Calendar.new(Time.new(2018, 6, 17))

        expect(cal.year).to eql(2)
      end

      it "is on year 3 after 24 weeks and one day" do
        cal = Calendar.new(Time.new(2018, 6, 18))

        expect(cal.year).to eql(3)
      end

      it "transcends one gregorian year" do
        cal = Calendar.new(Time.new(2019, 1, 1))

        expect(cal.year).to eql(5)
      end
    end

    context "day" do
      it "is day 1 for the first day" do
        cal = Calendar.new(Time.new(2018, 1, 1))

        expect(cal.day).to eql(1)
      end

      it "is day 84 for the last day of the first year" do
        cal = Calendar.new(Time.new(2018, 3, 25))

        expect(cal.day).to eql(84)
      end

      it "is day 1 for the first day of the second year" do
        cal = Calendar.new(Time.new(2018, 3, 26))

        expect(cal.day).to eql(1)
      end

      it "is day 1 for the first day of the third year" do
        cal = Calendar.new(Time.new(2018, 6, 18))

        expect(cal.day).to eql(1)
      end

      it "transcends one gregorian year" do
        cal = Calendar.new(Time.new(2019, 1, 1))

        expect(cal.day).to eql(30)
      end
    end

    context "week" do
      it "is week 1 for the first day" do
        cal = Calendar.new(Time.new(2018, 1, 1))

        expect(cal.week).to eql(1)
      end

      it "is week 1 for the last day of week 1" do
        cal = Calendar.new(Time.new(2018, 1, 7))

        expect(cal.week).to eql(1)
      end

      it "is week 1 for the first day of week 2" do
        cal = Calendar.new(Time.new(2018, 1, 8))

        expect(cal.week).to eql(2)
      end

      it "is week 2 for the last day of week 2" do
        cal = Calendar.new(Time.new(2018, 1, 14))

        expect(cal.week).to eql(2)
      end

      it "is week 3 for the first day of week 3" do
        cal = Calendar.new(Time.new(2018, 1, 15))

        expect(cal.week).to eql(3)
      end

      it "is week 1 for the first day of week 1 on year 2" do
        cal = Calendar.new(Time.new(2018, 3, 26))

        expect(cal.week).to eql(1)
      end

      it "is week 3 for the first day of week 3 on year 2" do
        cal = Calendar.new(Time.new(2018, 6, 18))

        expect(cal.year).to eql(3)
      end

      it "transcends one gregorian year" do
        cal = Calendar.new(Time.new(2019, 1, 1))

        expect(cal.week).to eql(5)
      end
    end
  end
end
