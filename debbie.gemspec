# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'debbie/version'

Gem::Specification.new do |spec|
  spec.name          = "debbie"
  spec.version       = Debbie::VERSION
  spec.authors       = ["Federico Iachetti"]
  spec.email         = ["iachetti.federico@gmail.com"]

  spec.summary       = "Calendar app"
  spec.description   = "Calendar app"
  spec.homepage      = "https://gitlab.com/fiachetti/debbie"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.14"
  spec.add_development_dependency "rake", "~> 12.3.0"
  spec.add_development_dependency "rspec", "~> 3.7.0"
  spec.add_development_dependency 'simplecov', "~> 0.14.1"
  spec.add_development_dependency "awesome_print", "~> 1.8.0"
end
